fetch('obj.json')
.then(el => el.json())
.then(el =>{
    $('#col').text(
        `${el.name} is a ${el.job}, and her skill is ${el.skills[0].skill}.`
    )
})
.catch(error => console.error(error))